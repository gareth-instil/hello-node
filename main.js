require('http')
    .createServer((request, response) => {
        console.log('Received request for URL: ' + request.url);
        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end('Hello World!');
    })
    .listen(8080, () => console.log('Ready and waiting...'));
